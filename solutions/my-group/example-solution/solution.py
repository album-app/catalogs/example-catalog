from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/

env_file = """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
"""


def run():
    from album.runner.api import get_args
    args = get_args()
    print("Hi", str(args.name), "nice to meet you!")


setup(
    group="my-group",
    name="example-solution",
    version="0.1.0",
    title="Example album solution",
    description="An Album solution example for running Python code, including all existing metadata parameters.",
    solution_creators=["author1", "author2"],
    cite=[{
        "text": "Citation1 text",
        "doi": "citation1/doi",
        "url": "https://citation1.url"
    }, {
        "text": "Citation2 text",
        "doi": "citation2/doi",
        "url": "https://citation2.url"
    }],
    doi="Solution DOI",
    acknowledgement="institute, grant..",
    tags=["template", "python"],
    license="unlicense",
    documentation=["documentation.md"],
    changelog="fixes:\n- fix1\n- fix2\n\nfeatures:\n- feature1\n- feature2",
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.5.1",
    args=[{
        "name": "name",
        "type": "string",
        "default": "Bugs Bunny",
        "description": "How to you want to be addressed?"
    }],
    custom={
        "custom_value1": "myvalue1",
        "custom_value2": 100
    },
    run=run,
    dependencies={'environment_file': env_file}
)
